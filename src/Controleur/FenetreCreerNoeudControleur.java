package Controleur;

import Modele.*;
import Vue.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class FenetreCreerNoeudControleur implements MouseListener, Observer {

	private Component Composant;
	private Modele Modele;
	private FenetreCreerNoeud FenetreCreerNoeud;
	private Integer x;
	private Integer y;
	private String NomNoeud;
	private Color color;
	private JTextField TextField;
	private Boolean inter;
	private Boolean LDP;
	private Boolean LDC;
	private JRadioButton boutonInter;
	private JRadioButton boutonLDP;
	private JRadioButton boutonLDC;

	public FenetreCreerNoeudControleur(FenetreCreerNoeud FenetreCreerNoeud, Component Composant, Modele Modele, Integer x, Integer y, JTextField TextField, JRadioButton boutonInter, JRadioButton boutonLDC, JRadioButton boutonLDP) {
		
		this.FenetreCreerNoeud=FenetreCreerNoeud;
		this.Composant=Composant;
		this.Modele=Modele;
		this.x=x;
		this.y=y;
		this.Modele.addObserver(this);
		this.TextField=TextField;
		this.boutonInter=boutonInter;
		this.boutonLDP=boutonLDP;
		this.boutonLDC=boutonLDC;
	}
	
	public void mouseClicked(MouseEvent e) {
				
		if(this.Composant.getName()=="valider") {
			
			this.inter = boutonInter.isSelected();
			this.LDC = boutonLDC.isSelected();
			this.LDP = boutonLDP.isSelected();
			
			if(this.LDC == true) {
				
				this.inter = boutonInter.isSelected();
				this.color=Color.RED;
				this.Modele.AddNoeud(this.TextField.getText(), this.x, this.y, this.color, true, false, false);
 			}
			else if(this.LDP == true) {
				
				this.color=Color.BLUE;
				this.Modele.AddNoeud(this.TextField.getText(), this.x, this.y, this.color, false, true, false);

			}
			else if(this.inter == true) {
				
				this.color=Color.ORANGE;
				this.Modele.AddNoeud(this.TextField.getText(), this.x, this.y, this.color, false, false, true);

			}
			else {
				
				this.color=Color.BLACK;
				this.Modele.AddNoeud(this.TextField.getText(), this.x, this.y, this.color, false, false, false);
				

			}
			
			this.FenetreCreerNoeud.dispose();
		}
		else if(this.Composant.getName()=="annuler") {
					
			this.FenetreCreerNoeud.dispose();
		}
		else if(this.Composant.getName()=="boutonLDC") {
			
			if(this.Modele.getStatutBoutonLDC()==false) {
				
				this.Modele.setBoutonLDCChecked(true);
				this.Modele.setBoutonLDPChecked(false);
				this.Modele.setBoutonInterChecked(false);
			}
		}
		else if(this.Composant.getName()=="boutonLDP") {
			
			if(this.Modele.getStatutBoutonLDP()==false) {
				
				this.Modele.setBoutonLDPChecked(true);
				this.Modele.setBoutonLDCChecked(false);
				this.Modele.setBoutonInterChecked(false);
			}
		}
		else if(this.Composant.getName()=="boutonInter") {
			
			if(this.Modele.getStatutBoutonInter()==false) {
				
				this.Modele.setBoutonInterChecked(true);
				this.Modele.setBoutonLDPChecked(false);
				this.Modele.setBoutonLDCChecked(false);
			}
		}		
	}
	
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}

	public void update(Observable o, Object arg) {}
}
