package Controleur;

import Vue.*;
import edu.uci.ics.jung.algorithms.layout.StaticLayout;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.AnimatedPickingGraphMousePlugin;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.GraphMouseListener;
import edu.uci.ics.jung.visualization.control.GraphMousePlugin;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse.Mode;
import edu.uci.ics.jung.visualization.control.PickingGraphMousePlugin;
import edu.uci.ics.jung.visualization.control.PluggableGraphMouse;
import Modele.*;

import javax.swing.* ;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Point2D;
import java.util.Observable;
import java.util.Observer;

public class FenetrePrincipaleControleur implements MouseListener, Observer {

	private Component Composant;
	private Modele Modele;
	private DefaultModalGraphMouse<Noeud, Arc> modalMouse;
	
	public FenetrePrincipaleControleur(Component Composant, Modele Modele) {
		
		this.Composant=Composant;
		this.Modele=Modele;
		this.Modele.addObserver(this);
	}
	
	public void mouseClicked (MouseEvent e) {
		
		if(this.Composant.getBackground() != Color.lightGray && this.Composant.getName() != "mapPannel" && this.Modele.getButtonPushed()==false) {
		
			this.Composant.setBackground(Color.lightGray);
			((JComponent) this.Composant).setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.BLUE));
			this.Modele.setButtonPushedStatut(true);
			
			switch(this.Composant.getName())
			{
				case "Creer Noeud" :
					this.Modele.setStatutBoutonCreerNoeud(true);
					break;
				case "Creer Arc" :
					this.Modele.setStatutBoutonCreerArc(true);
					FenetreCreerArc Fenetre = new FenetreCreerArc(this.Modele, this.Composant);
					break;
				case "Ajout Bateau" :
					this.Modele.setStatutBoutonAjouterBateau(true);
					break;
				case "Modifier" :
					this.Modele.setStatutBoutonModifier(true);
					this.modalMouse = new DefaultModalGraphMouse<Noeud, Arc>();
					this.modalMouse.setMode(Mode.PICKING);
					this.Modele.getVV().setGraphMouse(this.modalMouse);
					break;
				case "Raz" :
					this.Modele.setStatutBoutonRAZ(true);
					break;
				case "Lancer Simulation" :
					this.Modele.setStatutBoutonLancerSimulation(true);
					break;
				case "Supprimer" :
					this.Modele.setStatutBoutonSupprimer(true);
					break;
			}
		}
		else if(this.Composant.getBackground() == Color.lightGray && this.Composant.getName() != "mapPannel" && this.Modele.getButtonPushed()==true) {
			
			this.Composant.setBackground(Color.WHITE);
			((JComponent) this.Composant).setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLUE));
			this.Modele.setButtonPushedStatut(false);
			
			switch(this.Composant.getName())
			{
				case "Creer Noeud" : 			this.Modele.setStatutBoutonCreerNoeud(false);
												break;
				case "Creer Arc" : 				this.Modele.setStatutBoutonCreerArc(false);
												break;
				case "Ajout Bateau" : 			this.Modele.setStatutBoutonAjouterBateau(false);
												break;
				case "Modifier" :				this.Modele.getVV().setGraphMouse(null);
												this.Modele.setStatutBoutonModifier(false);
												break;
				case "Raz" :					this.Modele.setStatutBoutonRAZ(false);
												break;
				case "Lancer Simulation" :		this.Modele.setStatutBoutonLancerSimulation(false);
												break;
				case "Supprimer" : 				this.Modele.setStatutBoutonSupprimer(false);
												break;
			}
		}		
	}
	
	public void mousePressed(MouseEvent e) { }
	public void mouseReleased (MouseEvent e) { }
	public void mouseEntered (MouseEvent e) { }
	public void mouseExited(MouseEvent e) { }
	public void mouseMoved(MouseEvent e) { }

	public void update(Observable o, Object arg) { }
}
