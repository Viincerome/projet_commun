package Controleur;

import Vue.*;
import Modele.*;

import javax.swing.* ;
import java.awt.*;
import java.awt.event.*;
import java.util.Observable;
import java.util.Observer;

public class FenetreCreerArcControleur implements MouseListener, Observer {
	
	private Modele modele;
	private Component Composant;
	private FenetreCreerArc FenetreCreerArc;
	private JComboBox pointUnBox;
	private JComboBox pointDeuxBox;
	private JComboBox urBox;
	private JTextField nomArcText;
	private JTextField speedText;
	private String noeudDepart;
	private String noeudArrive;
	private String typeUr;
	private Integer idDepart;
	private Integer idArrive;
	private Noeud depart;
	private Noeud arrivee;
	
	public FenetreCreerArcControleur(FenetreCreerArc FenetreCreerArc, Modele m, Component Composant, JComboBox pointUnBox, JComboBox pointDeuxBox, JComboBox urBox, JTextField nomArcText, JTextField speedText) {
			
			this.modele=m;
			this.FenetreCreerArc=FenetreCreerArc;
			this.Composant=Composant;
			this.pointUnBox=pointUnBox;
			this.pointDeuxBox=pointDeuxBox;
			this.urBox=urBox;
			this.nomArcText=nomArcText;
			this.speedText=speedText;
			this.noeudDepart=(String) pointUnBox.getSelectedItem();
			this.noeudArrive=(String) pointDeuxBox.getSelectedItem();			
			this.modele.addObserver(this);
		}
	
	public void mouseClicked(MouseEvent e) {
		
		this.typeUr=(String) urBox.getSelectedItem();
		Component Composant2 = (Component)e.getSource();

		if(Composant2.getName()=="Valider"){
			
			this.noeudDepart=(String)pointUnBox.getSelectedItem();
			this.noeudArrive=(String)pointDeuxBox.getSelectedItem();
			
			this.idDepart=this.modele.trouverNomNoeud(this.noeudDepart);
			this.idArrive=this.modele.trouverNomNoeud(this.noeudArrive);
			this.depart = this.modele.trouverNoeud(idDepart);
			this.arrivee = this.modele.trouverNoeud(idArrive);

			
			System.out.println("TYPE UR = " + this.typeUr);
			switch(this.typeUr) {
			
				case "UR simple":
					this.modele.ajouterArc (this.depart, this.arrivee, 5, false);
					this.modele.ajouterArc (this.arrivee, this.depart, 5, false);
					break;
					
				case "UR courant":
					this.modele.ajouterArc (this.depart, this.arrivee, 5, false);
					break;
					
				case "UR double":
					this.modele.ajouterArc (this.depart, this.arrivee, 8, false);
					this.modele.ajouterArc (this.arrivee, this.depart, 8, false);
					break;		
			}
			
			
			this.Composant.setBackground(Color.GRAY);
			
			this.modele.setStatutBoutonCreerArc(false);
			this.modele.setButtonPushedStatut(false);
			
			this.FenetreCreerArc.dispose();
		}
	}
	
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	
	public void update(Observable o, Object arg) {}
}
