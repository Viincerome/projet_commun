package Controleur;

import Vue.*;
import Modele.*;

import javax.swing.* ;
import java.awt.*;
import java.awt.event.*;
import java.util.Observable;
import java.util.Observer;

public class MapPanelControleur implements MouseListener, Observer {
	
	private Component composant;
	private Modele Modele;
	
	public MapPanelControleur(Component composant, Modele Modele) {
		
		this.composant=composant;
		this.Modele=Modele;
		this.Modele.addObserver(this);
	}

	public void mouseClicked(MouseEvent e) {
						
		if(this.composant.getName()=="mapPannel") {
			
			if(this.Modele.getStatutBoutonCreerNoeud()==true) {
								
				Integer x = e.getX();			
				Integer y = e.getY();
				
				FenetreCreerNoeud Fenetre = new FenetreCreerNoeud(this.Modele, x, y);
			}
			else if(this.Modele.getStatutBoutonCreerArc()==true) {
				
			}
			else if(this.Modele.getStatutBoutonAjouterBateau()==true) {
				
			}
			else if(this.Modele.getStatutBoutonModifier()==true) {
				
			}
			else if(this.Modele.getStatutBoutonRAZ()==true) {
				
			}
			else if(this.Modele.getStatutBoutonLancerSimulation()==true) {
				
			}
			else if(this.Modele.getStatutBoutonSupprimer()==true) {
				
			}
		}
	}
	
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}

	public void update(Observable o, Object arg) {}
}
