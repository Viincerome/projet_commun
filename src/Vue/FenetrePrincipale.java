package Vue;

import Modele.*;
import Controleur.*;
import edu.uci.ics.jung.algorithms.layout.AbstractLayout;
import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.layout.StaticLayout;
import edu.uci.ics.jung.samples.SimpleGraphDraw;
import edu.uci.ics.jung.visualization.BasicVisualizationServer;
import edu.uci.ics.jung.visualization.DefaultVisualizationModel;
import edu.uci.ics.jung.visualization.VisualizationModel;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.EditingModalGraphMouse;
import edu.uci.ics.jung.visualization.control.GraphMouseListener;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.control.PickingGraphMousePlugin;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse.Mode;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.layout.PersistentLayoutImpl;
import edu.uci.ics.jung.visualization.picking.PickedState;

import javax.swing.*;

import org.apache.commons.collections15.Factory;

//import com.mxgraph.swing.mxGraphComponent;

import java.awt.event.*;
import java.awt.geom.Point2D;
import java.util.Observable;
import java.util.Observer;
import java.awt.*;

public class FenetrePrincipale extends JFrame implements Observer {

	private JButton creerNoeud;
	private JButton creerArc;
	private JButton ajoutBateau;
	private JButton modifier;
	private JButton raz;
	private JButton lancerSimulation;
	private JButton supprimer;
	private Color couleurParDefaut;
	
	private JPanel buttonsPanel;
	private JPanel mapPanel;
	private Modele Modele;
		
	@SuppressWarnings("unchecked")
	public FenetrePrincipale(Modele Modele) {
		
		this.Modele=Modele;
		this.Modele.addObserver(this);
		this.creerNoeud = new JButton("Cr�er Noeud");
		this.creerArc = new JButton("Cr�er Arc");
		this.ajoutBateau = new JButton("Ajout Bateau");
		this.modifier = new JButton("Modifier");
		this.raz = new JButton("Remise � Z�ro");
		this.lancerSimulation = new JButton("Lancer la Simulation");
		this.supprimer = new JButton("Supprimer");
		this.couleurParDefaut = Color.WHITE;
		
		this.Modele.getVV().setName("mapPannel");
		this.Modele.getVV().setBackground(Color.lightGray);
		this.Modele.getVV().setBorder(BorderFactory.createMatteBorder(1, 2, 1, 1, Color.BLACK));
        this.Modele.getVV().getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line<Noeud,Arc>());
		this.Modele.getVV().addMouseListener(new MapPanelControleur(this.Modele.getVV(), this.Modele));
				
		this.buttonsPanel = new JPanel();
		this.mapPanel = new JPanel();

        this.setLocationRelativeTo(null);
		
		this.creerNoeud.setPreferredSize(new Dimension(175, 50));
		this.creerNoeud.setBackground(couleurParDefaut);
		this.creerNoeud.setName("Creer Noeud");
		this.creerNoeud.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLUE));
		this.creerArc.setPreferredSize(new Dimension(175, 50));
		this.creerArc.setBackground(couleurParDefaut);
		this.creerArc.setName("Creer Arc");
		this.creerArc.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLUE));
		this.ajoutBateau.setPreferredSize(new Dimension(175, 50));
		this.ajoutBateau.setBackground(couleurParDefaut);
		this.ajoutBateau.setName("Ajout Bateau");
		this.ajoutBateau.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLUE));
		this.modifier.setPreferredSize(new Dimension(175, 50));
		this.modifier.setBackground(couleurParDefaut);
		this.modifier.setName("Modifier");
		this.modifier.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLUE));
		this.raz.setPreferredSize(new Dimension(175, 50));
		this.raz.setBackground(couleurParDefaut);
		this.raz.setName("Raz");
		this.raz.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLUE));
		this.lancerSimulation.setPreferredSize(new Dimension(175, 50));
		this.lancerSimulation.setBackground(couleurParDefaut);
		this.lancerSimulation.setName("Lancer Simulation");
		this.lancerSimulation.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLUE));
		this.supprimer.setPreferredSize(new Dimension(175, 50));
		this.supprimer.setBackground(couleurParDefaut);
		this.supprimer.setName("Supprimer");
		this.supprimer.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLUE));
		
		this.buttonsPanel.setPreferredSize(new Dimension(200, 500));
		this.buttonsPanel.add(creerNoeud);		
		this.buttonsPanel.add(creerArc);
		this.buttonsPanel.add(ajoutBateau);		
		this.buttonsPanel.add(modifier);
		this.buttonsPanel.add(raz);		
		this.buttonsPanel.add(supprimer);
		this.buttonsPanel.add(lancerSimulation);		
		add(this.buttonsPanel, BorderLayout.WEST);
		
        this.getContentPane().add(this.Modele.getVV(), BorderLayout.CENTER);
						
		this.creerNoeud.addMouseListener(new FenetrePrincipaleControleur(this.creerNoeud, this.Modele));
		this.creerArc.addMouseListener(new FenetrePrincipaleControleur(this.creerArc, this.Modele));
		this.ajoutBateau.addMouseListener(new FenetrePrincipaleControleur(this.ajoutBateau, this.Modele));
		this.raz.addMouseListener(new FenetrePrincipaleControleur(this.raz, this.Modele));
		this.modifier.addMouseListener(new FenetrePrincipaleControleur(this.modifier, this.Modele));
		this.lancerSimulation.addMouseListener(new FenetrePrincipaleControleur(this.lancerSimulation, this.Modele));
		this.supprimer.addMouseListener(new FenetrePrincipaleControleur(this.supprimer, this.Modele));
		
		this.pack();
		this.setVisible(true);
		this.setSize(1500, 1000);
		this.setLocation(0, 0);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
	}

	public void update(Observable o, Object arg) {
		
		for (Noeud n : this.Modele.getGraph().getVertices()) {
			
			Integer x = (int) this.Modele.getLayout().getX(n);
			Integer y = (int) this.Modele.getLayout().getY(n);
			
			System.out.println("X = " + x);
			
			n.setPosX(x);
			n.setPosY(y);
			
			Point2D p = new Point(x,y);
			this.Modele.getLayout().setLocation(n, p);
		}
		
		for(Arc a : this.Modele.getGraph().getEdges())
		{
			
			Noeud n1 = a.getDepart();
			Noeud n2 = a.getArrivee();
			

			Integer x1 = (int) this.Modele.getLayout().getX(n1);
			Integer y1 = (int) this.Modele.getLayout().getY(n1);
			
			Integer x2 = (int) this.Modele.getLayout().getX(n2);
			Integer y2 = (int) this.Modele.getLayout().getY(n2);

			n1.setPosX(x1);
			n1.setPosY(y1);
			
			n2.setPosX(x2);
			n2.setPosY(y2);
			

//			Point2D p = new Point(x1,y1);
//			this.Modele.getLayout().setLocation(n1, p);
//
//			Point2D p2 = new Point(x2,y2);
//			this.Modele.getLayout().setLocation(n2, p2);
		
			System.out.println("Coordonn�es D�part  = " + x1 + " + " + y1);
			System.out.println("Coordonn�es Arriv�e  = " + x2 + " + " + y2);

		}
			
		this.repaint();
	}
}


