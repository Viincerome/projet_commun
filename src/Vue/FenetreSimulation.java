package Vue;

import Modele.*;
import Controleur.*;

import javax.swing.*;

import java.awt.event.*;
import java.util.Observable;
import java.util.Observer;
import java.awt.*;

public class FenetreSimulation extends JFrame implements Observer {

	private JButton congestion;
	private JButton listeBateau;
	private JButton details;
	private Horloge horloge;
	private JButton remiseZero;
	private JButton pause;
	private JButton avancer;
	private JButton reculer;
	private JButton accelerer;
	private JButton decelerer;
	private JButton play;
	private JButton retour;

	private Color couleurParDefaut;
	
	private JPanel buttonsPanel;
	private JPanel mapPanel;
	
	private Modele Modele;
		

	public FenetreSimulation(Modele Modele) {
		
		this.Modele=Modele;
		this.Modele.addObserver(this);
		this.congestion = new JButton("Congestion");
		this.listeBateau = new JButton("Liste des Bateaux");
		this.details = new JButton("D�tails");
		this.horloge = new Horloge();
		this.remiseZero = new JButton("Remise � Z�ro");
		this.pause = new JButton(new ImageIcon("Images/player/pause.png"));
		this.avancer = new JButton(new ImageIcon("Images/player/avance.png"));
		this.reculer = new JButton(new ImageIcon("Images/player/recule.png"));
		this.accelerer = new JButton(new ImageIcon("Images/player/avanceRapide.png"));
		this.decelerer = new JButton(new ImageIcon("Images/player/reculeRapide.png"));
		this.play = new JButton(new ImageIcon("Images/player/play.png"));
		this.retour = new JButton("Retour � la conf");
		
		this.buttonsPanel = new JPanel();
		this.mapPanel = new JPanel();
		
		this.setLayout(new BorderLayout());
		
		this.congestion.setPreferredSize(new Dimension(175, 50));	//50
		this.congestion.setName("Congestion");
		this.listeBateau.setPreferredSize(new Dimension(175, 50));	//100
		this.listeBateau.setName("Liste des Bateaux");
		this.details.setPreferredSize(new Dimension(175, 50));		//150
		this.details.setName("D�tails");
		this.horloge.getClockPanel().setPreferredSize(new Dimension(175, 300));		//325
		//this.horloge.setBackground(Color.white);
		
		this.pause.setPreferredSize(new Dimension(87, 40));		//375
		this.pause.setName("Pause");
		this.play.setPreferredSize(new Dimension(87, 40));		//375
		this.play.setName("Play");
		this.play.setBackground(Color.gray);
		this.reculer.setPreferredSize(new Dimension(87, 40));		//415
		this.reculer.setName("Reculer");
		this.avancer.setPreferredSize(new Dimension(87, 40));		//415
		this.avancer.setName("Avancer");
		this.decelerer.setPreferredSize(new Dimension(87, 40));		//455
		this.decelerer.setName("Decelerer");
		this.accelerer.setPreferredSize(new Dimension(87, 40));		//455
		this.accelerer.setName("Accelerer");
		this.remiseZero.setPreferredSize(new Dimension(175, 50));		//505
		this.remiseZero.setName("Stop");
		this.retour.setPreferredSize(new Dimension(175, 50));		//555
		this.retour.setName("Retour");
		
		
		
		this.buttonsPanel.setPreferredSize(new Dimension(200, 750));
		this.buttonsPanel.add(congestion);		
		this.buttonsPanel.add(listeBateau);
		this.buttonsPanel.add(details);	
		this.buttonsPanel.add(horloge.getClockPanel());
		this.buttonsPanel.add(pause);
		this.buttonsPanel.add(play);
		this.buttonsPanel.add(reculer);
		this.buttonsPanel.add(avancer);
		this.buttonsPanel.add(decelerer);
		this.buttonsPanel.add(accelerer);
		this.buttonsPanel.add(remiseZero);
		this.buttonsPanel.add(retour);
		
		add(this.buttonsPanel, BorderLayout.WEST);
		
		this.mapPanel.setPreferredSize(new Dimension(400, 500));
		this.mapPanel.setBackground(Color.WHITE);
		this.mapPanel.setLayout(null);
		this.mapPanel.setName("mapPannel");
		add(this.mapPanel, BorderLayout.CENTER);
		
		this.mapPanel.addMouseListener(new MapPanelControleur(this.mapPanel, this.Modele));
		this.pause.addMouseListener(new FenetreSimulationControleur(this, this.Modele));
//		this.congestion.addMouseListener(new FenetrePrincipaleControleur(this.congestion, this.Modele));
//		this.listeBateau.addMouseListener(new FenetrePrincipaleControleur(this.listeBateau, this.Modele));
//		this.details.addMouseListener(new FenetrePrincipaleControleur(this.details, this.Modele));
//		this.raz.addMouseListener(new FenetrePrincipaleControleur(this.raz, this.Modele));
//		this.modifier.addMouseListener(new FenetrePrincipaleControleur(this.modifier, this.Modele));
//		this.lancerSimulation.addMouseListener(new FenetrePrincipaleControleur(this.lancerSimulation, this.Modele));
//		this.supprimer.addMouseListener(new FenetrePrincipaleControleur(this.supprimer, this.Modele));

		setVisible(true);
		this.setSize(1500, 800);
		//this.setLocation(400, 200);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
	}

	public JButton getPause() {
		return pause;
	}

	public void setPause(JButton pause) {
		this.pause = pause;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		
	}

}

