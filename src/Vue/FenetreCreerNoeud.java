package Vue;

import Modele.*;
import Controleur.*;

import javax.swing.* ;
import java.awt.*;
import java.awt.event.*;
import java.util.Observable;
import java.util.Observer;

public class FenetreCreerNoeud extends JFrame implements Observer {

	private JLabel nomNoeud;
	private JTextField nomNoeudText;
	private JRadioButton buttonLDP;
	private JRadioButton buttonLDC;
	private JRadioButton buttonInter;
	private JButton valider;
	private JButton annuler;
	private JPanel panelBouton;
	private ButtonGroup groupe;
	private GridBagConstraints gbc;
	
	private Modele Modele;
	private Integer x;
	private Integer y;
	
	public FenetreCreerNoeud(Modele Modele, Integer x, Integer y) {             
		
		this.Modele=Modele;
		this.x=x;
		this.y=y;
		
		this.setSize(400, 250);
		this.setLocation(x, y);		
		
		this.setLayout(new GridBagLayout());
		this.setAlwaysOnTop(true);
		
		this.nomNoeud = new JLabel("Nom du Noeud");
		this.nomNoeudText = new JTextField();
							
		this.buttonLDP = new JRadioButton("Lieu de Production");
		this.buttonLDP.setName("boutonLDP");
		this.buttonLDC = new JRadioButton("Lieu de Consommation");
		this.buttonLDC.setName("boutonLDC");
		this.buttonInter = new JRadioButton("Intersection",true);
		this.buttonInter.setName("boutonInter");
		
		this.valider = new JButton("Valider");
		this.valider.setName("valider");
		this.annuler = new JButton("Annuler");
		this.annuler.setName("annuler");
		
		this.panelBouton = new JPanel();
		
		this.panelBouton.add(buttonInter);
		this.panelBouton.add(buttonLDC);
		this.panelBouton.add(buttonLDP);
		
		this.groupe = new ButtonGroup();

		this.nomNoeudText.setPreferredSize(new Dimension(200, 50));
		
		this.gbc = new GridBagConstraints();
		
		this.groupe.add(buttonLDP);		
		this.groupe.add(buttonLDC);		
		this.groupe.add(buttonInter);
		
		this.gbc.gridx = 0;		
		this.gbc.gridy = 0;		
		this.gbc.gridwidth = 1;		
		this.gbc.gridheight = 1;
		this.add(nomNoeud, gbc);
		
		this.gbc.gridx = 1;		
		this.gbc.gridy = 0;		
		this.gbc.gridwidth = 1;		
		this.gbc.gridheight = 1;
		this.gbc.insets = new Insets(5, 0, 5, 0);		
		this.add(nomNoeudText, gbc);	
		
		this.gbc.gridx = 0;		
		this.gbc.gridy = 1;		
		this.gbc.gridwidth = 1;		
		this.gbc.gridheight = 1;
		this.gbc.anchor = GridBagConstraints.LINE_START;		
		this.add(buttonLDP, gbc);

		this.gbc.gridx = 0;		
		this.gbc.gridy = 2;		
		this.gbc.gridwidth = 1;		
		this.gbc.gridheight = 1;
		this.gbc.anchor = GridBagConstraints.LINE_START;		
		this.add(buttonLDC, gbc);
		
		this.gbc.gridx = 0; 		
		this.gbc.gridy = 3;		
		this.gbc.gridwidth = 1;		
		this.gbc.gridheight = 1;
		this.gbc.anchor = GridBagConstraints.LINE_START;		
		this.add(buttonInter, gbc);
		
		this.gbc.gridx = 0;		
		this.gbc.gridy = 4;		
		this.gbc.gridwidth = 1;		
		this.gbc.gridheight = 1;
		
		this.panelBouton.add(valider);
		this.panelBouton.add(annuler);
		this.add(panelBouton, gbc);
		
//		this.buttonInter.addMouseListener(new FenetreCreerNoeudControleur(this, this.buttonInter, this.Modele, this.x, this.y, this.nomNoeudText));
//		this.buttonLDP.addMouseListener(new FenetreCreerNoeudControleur(this, this.buttonLDP, this.Modele, this.x, this.y, this.nomNoeudText));
//		this.buttonLDC.addMouseListener(new FenetreCreerNoeudControleur(this, this.buttonLDC, this.Modele, this.x, this.y, this.nomNoeudText));
		this.valider.addMouseListener(new FenetreCreerNoeudControleur(this, this.valider, this.Modele, this.x, this.y, this.nomNoeudText, this.buttonInter,this.buttonLDC,this.buttonLDP));
		this.annuler.addMouseListener(new FenetreCreerNoeudControleur(this, this.annuler, this.Modele, this.x, this.y, this.nomNoeudText, this.buttonInter,this.buttonLDC,this.buttonLDP));

		this.setVisible(true);
		this.setResizable(false);
	}

	public void update(Observable o, Object arg) {}
}
