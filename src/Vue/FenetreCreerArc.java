package Vue;

import Modele.*;
import Controleur.*;

import javax.swing.*;

import Controleur.FenetreCreerNoeudControleur;

import java.awt.*;
import java.awt.List;
import java.awt.event.*;
import java.util.*;

public class FenetreCreerArc extends JFrame implements Observer {

	private JLabel nomArc;
	private JLabel pointUn;
	private JLabel pointDeux;
	private JLabel speed;
	private JLabel uniteRiviere;
	private JComboBox<String> pointUnBox;
	private JComboBox<String> pointDeuxBox;
	private JComboBox<String> urBox;
	private JTextField nomArcText;
	private JTextField speedText;
	private JPanel panelBouton;
	private JButton valider;
	private JButton annuler;
	private Modele modele;
	private Component composant;
	private GridBagConstraints gbc;
	private ArrayList<String> tabNoeud;
	
	public FenetreCreerArc(Modele m,Component c) {
	
		this.modele=m;
		this.composant=c;
		this.modele.addObserver(this);
		
		this.setSize(400, 325);
		this.setLocation(0, 300);
		this.setLayout(new GridBagLayout());
		this.nomArc = new JLabel("Nom de l'arc");
		this.nomArcText = new JTextField();
		
		this.pointUn = new JLabel("Noeud 1");
//		this.tabNoeud = new String[this.modele.getListNoeuds().size()];
		this.tabNoeud = new ArrayList<String>();
		
		for(Noeud noeud : this.modele.getListNoeuds()) {
			
//			tabNoeud[noeud.getId()]=noeud.getNom();
			tabNoeud.add(noeud.getNom());
		}
		
		this.pointUnBox = new JComboBox(tabNoeud.toArray());
		
		
		this.pointDeux = new JLabel("Noeud 2");
		this.pointDeuxBox = new JComboBox(tabNoeud.toArray());
		
		this.uniteRiviere = new JLabel("UR");
		String[] pur = {"UR simple", "UR courant","UR double"};
		this.urBox = new JComboBox(pur);
		
		this.speed = new JLabel("Vitesse de l'UR");
		this.speedText = new JTextField();
		
		
		this.panelBouton = new JPanel();
		this.valider = new JButton("Valider");
		this.annuler = new JButton("Annuler");
		this.valider.setName("Valider");
		this.annuler.setName("Annuler");

		nomArcText.setPreferredSize(new Dimension(200, 50));
		pointUnBox.setPreferredSize(new Dimension(200, 25));
		pointDeuxBox.setPreferredSize(new Dimension(200, 25));
		urBox.setPreferredSize(new Dimension(200, 25));
		speedText.setPreferredSize(new Dimension(200, 50));
		
		this.gbc = new GridBagConstraints();
		
		gbc.gridx = 0;
		gbc.gridy = 0;		
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		gbc.anchor = GridBagConstraints.LINE_START;
		this.add(nomArc, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 0;	
		gbc.gridwidth = 1;	
		gbc.gridheight = 1;
		gbc.insets = new Insets(5, 0, 5, 0);
		this.add(nomArcText, gbc);	
		
		gbc.gridx = 0;	
		gbc.gridy = 1;	
		gbc.gridwidth = 1;		
		gbc.gridheight = 1;
		gbc.anchor = GridBagConstraints.LINE_START;	
		this.add(pointUn, gbc);
		
		gbc.gridx = 1;	
		gbc.gridy = 1;	
		gbc.gridwidth = 1;		
		gbc.gridheight = 1;
		gbc.insets = new Insets(5, 0, 5, 0);
		this.add(pointUnBox, gbc);	

		gbc.gridx = 0;
		gbc.gridy = 2;	
		gbc.gridwidth = 1;		
		gbc.gridheight = 1;
		gbc.anchor = GridBagConstraints.LINE_START;		
		this.add(pointDeux, gbc);
		
		gbc.gridx = 1;	
		gbc.gridy = 2;	
		gbc.gridwidth = 1;	
		gbc.gridheight = 1;
		gbc.insets = new Insets(5, 0, 5, 0);	
		this.add(pointDeuxBox, gbc);	
				
		gbc.gridx = 0;	
		gbc.gridy = 3;	
		gbc.gridwidth = 1;	
		gbc.gridheight = 1;
		gbc.anchor = GridBagConstraints.LINE_START;		
		this.add(uniteRiviere, gbc);
		
		gbc.gridx = 1;	
		gbc.gridy = 3;	
		gbc.gridwidth = 1;	
		gbc.gridheight = 1;
		gbc.insets = new Insets(5, 0, 5, 0);	
		this.add(urBox, gbc);	
				
		gbc.gridx = 0;	
		gbc.gridy = 4;		
		gbc.gridwidth = 1;	
		gbc.gridheight = 1;
		gbc.anchor = GridBagConstraints.LINE_START;		
		this.add(speed, gbc);
		
		gbc.gridx = 1;	
		gbc.gridy = 4;	
		gbc.gridwidth = 1;	
		gbc.gridheight = 1;
		gbc.insets = new Insets(5, 0, 5, 0);	
		this.add(speedText, gbc);	
		
		gbc.gridx = 0;	
		gbc.gridy = 5;	
		gbc.gridwidth = 1;	
		gbc.gridheight = 1;
		panelBouton.add(valider);		
		panelBouton.add(annuler);	
		this.add(panelBouton, gbc);
		
		this.setVisible(true);
		this.setResizable(false);
		
		this.valider.addMouseListener(new FenetreCreerArcControleur(this,modele,composant,pointUnBox,pointDeuxBox,urBox,nomArcText,speedText));
		this.annuler.addMouseListener(new FenetreCreerArcControleur(this,modele,composant,pointUnBox,pointDeuxBox,urBox,nomArcText,speedText));
	}
	
	
	
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}
	
	

	

}
