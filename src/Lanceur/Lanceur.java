package Lanceur;
import Modele.*;
import Vue.*;
import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.BasicVisualizationServer;
import Controleur.*;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;

public class Lanceur {

	public static void main(String[] args) throws Exception {
		
		Modele modele = new Modele();
				
		FenetrePrincipale FP = new FenetrePrincipale(modele);
		FP.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
	}
}

