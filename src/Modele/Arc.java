package Modele;

import java.util.Observable;
import Modele.*;
import javax.swing.*;

//import org.graphstream.*;
//import org.graphstream.graph.Graph;
//import org.graphstream.graph.implementations.*;

import Controleur.*;

import java.awt.*;
import java.util.Observer;

public class Arc implements Observer {

	private Noeud depart;
	private Noeud arrivee;
	private Integer longueur;
	private Integer vitesse;
	private Double temps;
	private Boolean courant;
	private Modele modele;
	private Integer id;
	private double distance;
	
	public Arc (Modele m, Integer id, Noeud d, Noeud a, Integer v, Boolean c){
		
		this.modele=m;
		this.modele.addObserver(this);

		this.id=id;
		this.depart = d;
		this.arrivee = a;
		this.vitesse = v;
//		this.temps = l/v;
	}

	public Noeud getDepart() {
		
		return this.depart;
	}
	
	public Noeud getArrivee() {
		
		return this.arrivee;
	}
	
	public Integer getLongueur() {
		
		return this.longueur;
	}
	
	public void setLongueur(Integer p) {
		
		this.longueur = p;
	}
	
	public void setDepart(Noeud d) {
		
		this.depart = d;
	}
	
	public void setArrivee(Noeud a) {
		
		this.arrivee = a;
	}
	
	public void setVitesse(Integer v) {
		
		this.vitesse = v;
	}
	
	public Integer getId() {
		
		return this.id;
	}
	
	public void setId(Integer id) {
		
		this.id=id;
	}
	
	public Double getDistance(){
		
		int x_depart = this.depart.getPosX();
		int y_depart = this.depart.getPosY();
		
		int x_arrivee = this.arrivee.getPosX();
		int y_arrivee = this.arrivee.getPosY();
		
		this.distance = ((x_arrivee-x_depart)^2)+((y_arrivee-y_depart)^2);
		this.distance = Math.sqrt(distance);
		
		return distance;
	}
	
	public Double getTemps(){
		
		Double l = this.getDistance();
		
		this.temps = l/vitesse;
		
		return temps;
	}

	// METHODES 
	
	// Permet de tester l'�galit� entre deux Arcs (!!! le poids ne rentre pas en ligne de compte)
//	@Override
//	public boolean equals(Object obj)
//	{
//	    if(obj == null)
//	    {
//	    	return false;		    	
//	    }
//	    else
//	    {
//	    	return 	((Arc)obj).depart == this.depart &&	((Arc)obj).arrivee == this.arrivee ;
//	    }
//	}
	
	// affichage d'un arc
	@Override
	public String toString() {
		
		return "source = "+ this.depart +" -> destination = "+ this.arrivee +" : " + this.longueur + " : "+ this.vitesse +"\n";
	}

	public void update(Observable arg0, Object arg1) {
		
		System.out.println("Pos Depart Arc = " + this.depart.getPosX());
	}
	
	
	
	
}
