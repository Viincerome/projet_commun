package Modele;

import Modele.*;
import Controleur.*;
import javax.swing.*;

import Controleur.FenetrePrincipaleControleur;

import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class Noeud implements Observer {
	
	private Integer id;
	private Integer posX;
	private Integer posY;
	private boolean LDC;
	private boolean LDP;
	private boolean inter;
	private Color color;
	private String nom;
	private Modele modele;

	public Noeud (Modele modele, String n, Integer id, Integer x, Integer y, Color color, Boolean ldc, Boolean ldp, Boolean inter){
		
		this.modele=modele;
		this.modele.addObserver(this);
		
		this.nom = n;
		this.posX = x;
		this.posY = y;
		this.LDC = ldc;
		this.LDP = ldp;
		this.color = color;
		this.id=id;
	}

	public Integer getId(){
		
		return this.id;
	}

	public Integer getPosX(){
		
		return this.posX;
	}
	
	public Integer getPosY(){
		
		return this.posY;
	}
	
	public Boolean isLDC(){
		
		return this.LDC;
	}
	
	public Boolean isLDP(){
		
		return this.LDP;
	}
	
	public void setId(Integer i){
		
		this.id = i;
	}
	
	public void setPosX(Integer x){
		
		this.posX = x;
	}
	
	public void setPosY(Integer y){
		
		this.posY = y;
	}
	
	public Color getColor() {
		
		return color;
	}

	public void setColor(Color color) {
		
		this.color = color;
	}
	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
    
	public void update(Observable arg0, Object arg1) {
		
		System.out.println("Noeud =  " + this.posX + " ---- " + this.posY);

	}

	public void setX(Integer x) {
		
		this.posX=x;
	}
	
	public void setY(Integer y) {
		
		this.posY=y;
	}

}
