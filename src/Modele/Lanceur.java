//package Modele;
//
//import java.awt.Color;
//
//public class Lanceur {
//
//	public static void main(String[] args) throws Exception {
//		Graphe g = new Graphe();
//		g.ajouterSommet("a",5,5,true,false,Color.BLACK);
//		g.ajouterSommet("b",6,5,false,false,Color.BLACK);
//		g.ajouterSommet("c",7,5,false,false,Color.BLACK);
//		g.ajouterSommet("d",8,5,false,true,Color.BLACK);
//		g.ajouterSommet("e",9,5,true,false,Color.BLACK);
//					
//		g.ajouterArc(1, 2, 10,5,false);
//		g.ajouterArc(1, 4, 30,5,false);
//		g.ajouterArc(1, 5, 100,8,false);
//		g.ajouterArc(2, 3, 50,3,false);
//		g.ajouterArc(3, 5, 20,8,false);
//		g.ajouterArc(4, 3, 10,7,false);
//		g.ajouterArc(4, 5, 60,6,false);
//	
//		//AlgoBellman.Execution(g, 1, 5);
//		AlgoDijkstra.Execution(g, 1, 5);
//
//	}
//
//}
