package Modele;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;

import javax.swing.JLabel;
import javax.xml.transform.ErrorListener;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;

import org.apache.commons.collections15.TransformerUtils;

import Controleur.MapPanelControleur;

import Vue.*;
import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.layout.StaticLayout;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseMultigraph;
import edu.uci.ics.jung.graph.event.GraphEvent.Vertex;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.graph.util.Pair;
import edu.uci.ics.jung.visualization.BasicVisualizationServer;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.GraphMouseListener;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse.Mode;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.control.PickingGraphMousePlugin;
import edu.uci.ics.jung.visualization.picking.PickedState;


public class Modele extends Observable {

	private boolean boutonPushed;
	private boolean BoutonCreerNoeudPushed;
	private boolean BoutonCreerArcPushed;
	private boolean BoutonAjouterBateauPushed;
	private boolean BoutonModifierPushed;
	private boolean BoutonRAZPushed;
	private boolean BoutonLancerSimulationPushed;
	private boolean BoutonSupprimerPushed;
	
	private boolean BoutonLDCChecked;
	private boolean BoutonLDPChecked;
	private boolean BoutonInterChecked;
	
	private List<Integer>IdNoeuds;
	private List<Integer>IdArcs;
	private Integer IdMaxNoeuds;
	private Integer IdMaxArcs;
	
	private List<Noeud> ListNoeuds;
	private List<Arc> ListArcs;
	
	private SparseMultigraph<Noeud, Arc> graph;
	private StaticLayout<Noeud, Arc> layout;
	private VisualizationViewer<Noeud,Arc> vv;
	private DefaultModalGraphMouse<Noeud, Arc> modalMouse;	
	
	public Modele() {
		
		this.boutonPushed=false;
		this.BoutonCreerNoeudPushed=false;
		this.BoutonCreerArcPushed=false;
		this.BoutonAjouterBateauPushed=false;
		this.BoutonModifierPushed=false;
		this.BoutonRAZPushed=false;
		this.BoutonLancerSimulationPushed=false;
		this.BoutonSupprimerPushed=false;
		
		this.BoutonLDCChecked=false;
		this.BoutonLDPChecked=false;
		this.BoutonInterChecked=false;
		
		this.IdNoeuds=new ArrayList<Integer>();
		this.IdArcs=new ArrayList<Integer>();
		this.ListNoeuds = new ArrayList<Noeud>();
		this.ListArcs = new ArrayList<Arc>();
		
		this.IdMaxNoeuds=0;
		this.IdMaxArcs=0;
		
		this.graph = new SparseMultigraph<Noeud, Arc>();
		this.layout = new StaticLayout<Noeud, Arc>(this.graph);	
		this.vv = new VisualizationViewer<Noeud,Arc>(this.layout);
	}
	
	public boolean getButtonPushed() {
		
		return this.boutonPushed;
	}
	
	public void setButtonPushedStatut(boolean bool) {
		
		this.boutonPushed=bool;
		this.setChanged();
        this.notifyObservers();
	}
	
	public boolean getStatutBoutonCreerNoeud() {
		
		return this.BoutonCreerNoeudPushed;
	}
	
	public void setStatutBoutonCreerNoeud(boolean bool) {
		
		this.BoutonCreerNoeudPushed=bool;
		this.setChanged();
        this.notifyObservers();
	}
	
	public boolean getStatutBoutonCreerArc() {
		
		return this.BoutonCreerArcPushed;
	}
	
	public void setStatutBoutonCreerArc(boolean bool) {
		
		this.BoutonCreerArcPushed=bool;
		this.setChanged();
        this.notifyObservers();
	}
	
	public boolean getStatutBoutonAjouterBateau() {
		
		return this.BoutonAjouterBateauPushed;
	}
	
	public void setStatutBoutonAjouterBateau(boolean bool) {
		
		this.BoutonAjouterBateauPushed=bool;
		this.setChanged();
        this.notifyObservers();
	}
	
	public boolean getStatutBoutonModifier() {
		
		return this.BoutonModifierPushed;
	}
	
	public void setStatutBoutonModifier(boolean bool) {
		
		this.BoutonModifierPushed=bool;
		this.setChanged();
        this.notifyObservers();
	}
	
	public boolean getStatutBoutonRAZ() {
		
		return this.BoutonRAZPushed;
	}
	
	public void setStatutBoutonRAZ(boolean bool) {
		
		this.BoutonRAZPushed=bool;
		this.setChanged();
        this.notifyObservers();
	}
	
	public boolean getStatutBoutonLancerSimulation() {
		
		return this.BoutonLancerSimulationPushed;		
	}
	
	public void setStatutBoutonLancerSimulation(boolean bool) {
		
		this.BoutonLancerSimulationPushed=bool;
		this.setChanged();
        this.notifyObservers();
	}
	
	public boolean getStatutBoutonSupprimer() {
		
		return this.BoutonSupprimerPushed;
	}
	
	public void setStatutBoutonSupprimer(boolean bool) {
		
		this.BoutonSupprimerPushed=bool;
		this.setChanged();
        this.notifyObservers();
	}
	
	public boolean getStatutBoutonLDC() {
		
		return this.BoutonLDCChecked;
	}
	
	public void setBoutonLDCChecked(boolean bool) {
		
		this.BoutonLDCChecked=bool;
		this.setChanged();
        this.notifyObservers();
	}
	
	public boolean getStatutBoutonLDP() {
		
		return this.BoutonLDPChecked;
	}
	
	public void setBoutonLDPChecked(boolean bool) {
		
		this.BoutonLDPChecked=bool;
		this.setChanged();
        this.notifyObservers();
	}
	
	public boolean getStatutBoutonInter() {
		
		return this.BoutonInterChecked;
	}
	
	public void setBoutonInterChecked(boolean bool) {
		
		this.BoutonInterChecked=bool;
		this.setChanged();
        this.notifyObservers();
	}
		
	public List<Integer> getListIdNoeuds() {
		
		return this.IdNoeuds;
	}
	
	public List<Noeud> getListNoeuds() {
		
		return this.ListNoeuds;
	}
	
	public List<Arc> getListArcs() {
		
		return this.ListArcs;
	}
	
	public Integer getIdMaxNoeud() {
		
		return this.IdMaxNoeuds;
	}
	
	public void setIdMaxNoeuds(Integer newIdMax) {
		
		this.IdMaxNoeuds=newIdMax;
		this.setChanged();
        this.notifyObservers();
	}
	
	public Integer getIdMaxArcs() {
		
		return this.IdMaxArcs;
	}
	
	public void setIdMaxArcs(Integer newIdMax) {
		
		this.IdMaxArcs=newIdMax;
		this.setChanged();
        this.notifyObservers();
	}
	
	public List<Integer> getListIdArcs() {
		
		return this.IdArcs;
	}
	
	public Integer index(Noeud n) {
		
		int k = this.ListNoeuds.size();
		
		for(int i=0; i<k ; i++) {
			
			if(n.getId()==this.ListNoeuds.get(i).getId()) {
				
				return i;
			}
		}
		
		return -1;
	}
	
	public Integer indexNom(Noeud n){
		
		int k = this.ListNoeuds.size();
		
		for(int i=0; i<k; i++) {
			
            if (n.getNom()==this.ListNoeuds.get(i).getNom()) {
        
            	return i;
            }
		}
        return -1;
	}
	
	public Integer trouverNomNoeud(String nom){
		
		return this.getListNoeuds().get(indexNom(new Noeud(this,nom, 1, 1, 1,Color.RED,true, true,true))).getId();
	}
	
	public Noeud trouverNoeud(Integer id) {
		
		return this.getListNoeuds().get(index(new Noeud(this, "blabla", id, 1, 1,Color.RED,true, true,true)));
	}

			
	public void AddNoeud(String nom, Integer x, Integer y, Color color, boolean LDC, boolean LDP, boolean Inter) {
		 	
			Noeud temp = new Noeud(this, nom, this.getIdMaxNoeud(), x, y, color, LDC, LDP, Inter);
			this.ListNoeuds.add(temp);
			this.IdNoeuds.add(this.getIdMaxNoeud());
			this.setIdMaxNoeuds(this.getIdMaxNoeud()+1);
			this.graph.addVertex(temp);

					
			Point2D p = new Point(temp.getPosX(),temp.getPosY());
			System.out.println(p);
			this.getLayout().setLocation(temp, p);
			this.setChanged();
	        this.notifyObservers();
	}
	
	
	
	public void ajouterArc (Noeud d, Noeud a, Integer v, Boolean c) {
			
		Arc temp = new Arc(this, this.getIdMaxArcs(), d, a, v, c);
		this.graph.addEdge(temp, d, a, EdgeType.DIRECTED);
		this.ListArcs.add(temp);
		this.IdNoeuds.add(this.getIdMaxArcs());
		this.setIdMaxArcs(this.getIdMaxArcs()+1);	
		this.setChanged();
        this.notifyObservers();
	}

	public StaticLayout<Noeud, Arc> getLayout() {

		return this.layout;
	}

	public VisualizationViewer<Noeud, Arc> getVV() {

		return this.vv;
	}

	public void setVV(VisualizationViewer<Noeud, Arc> vv) {
		
		this.vv=vv;
	}

	public void setLayout(StaticLayout<Noeud, Arc> layout) {
		
		this.layout=layout;
	}

	public SparseMultigraph<Noeud, Arc> getGraph() {
		
		return this.graph;
	}
}
