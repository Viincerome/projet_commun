package Modele;



import java.util.Observable;

public class Bateau extends Observable {
	private Integer identifiant;
	private Integer vitesse;
	private Integer tirant_eau;
	private String categorie;
	private Integer LDP;
	private Integer LDC;

	public Bateau (Integer id, Integer vit, Integer tir, String cat, Integer ldp, Integer ldc){
		this.identifiant = id;
		this.vitesse = vit;
		this.tirant_eau = tir;
		this.categorie = cat;
		this.LDP = ldp;
		this.LDC = ldc;
	}
	
	public Integer getId(){
		return this.identifiant;
	}
	public Integer getVitesse(){
		return this.vitesse;
	}
	public Integer getLDP(){
		return this.LDP;
	}
	public Integer getLDC(){
		return this.LDC;
	}
	public Integer getTirant(){
		return this.tirant_eau;
	}
	public String getCategorie(){
		return this.categorie;
	}
	
}
