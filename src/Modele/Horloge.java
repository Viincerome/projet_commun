package Modele;
import java.awt.*;

import javax.swing.*;

import java.util.GregorianCalendar;

public class Horloge
{
	public Horloge()	{
		
		calend=new GregorianCalendar();
		h=calend.get(GregorianCalendar.HOUR_OF_DAY);
		min=calend.get(GregorianCalendar.MINUTE);
		sec=calend.get(GregorianCalendar.SECOND);
		
		setClockPanel(new ClockPanel());
		
		clockThread=new ClockThread(this);
		clockThread.start();
		
	}/* Fin constructeur*/
	
	@SuppressWarnings("serial")
	public
	class ClockPanel extends JPanel 
	{
		public void paintComponent (Graphics g)
		{
			super.paintComponent(g);
			
			 xc=85;
			 yc=85;
		     int rayon=Math.min(xc,yc)*80/100;
		     
		     font= new Font("Times New Roman",0,15);
		     g.setFont(font);
			
			for(int i=1;i<=12;i++)
			{
			     double angle=i*Math.PI/6.0-Math.PI/2.0;
			     double x=xc+rayon*Math.cos(angle);
			     double y=50+yc+rayon*Math.sin(angle);
			      g.drawString(" "+i,(int)x,(int)y);
			}
			
			/* gestion et affichage de aiguilles*/
		//seconde	
		double anglesec=(sec*((Math.PI)/30.0)-(Math.PI/2.0));
		int xsf=xc+(int)(0.7*rayon*Math.cos(anglesec));
		int ysf=yc+(int)(0.7*rayon*Math.sin(anglesec));
		g.setColor(Color.black);
		g.drawLine(5+xc,50+yc,5+xsf,50+ysf);
			
		//minute
		double anglemin=(min*((Math.PI)/30.0)-(Math.PI/2.0));
		int xmf=xc+(int)(0.6*rayon*Math.cos(anglemin));
		int ymf=yc+(int)(0.6*rayon*Math.sin(anglemin));
		g.setColor(Color.red);
		g.drawLine(5+xc,50+yc,5+xmf,50+ymf);

		//heure
		double angleheure=(h*((2*Math.PI)/12.0)-(Math.PI/2.0));
		int xhf=xc+(int)(0.4*rayon*Math.cos(angleheure));
		int yhf=yc+(int)(0.4*rayon*Math.sin(angleheure));
		g.setColor(Color.blue);
		g.drawLine(5+xc,50+yc,5+xhf,50+yhf);
			
	
		}/* fin de paintComponent */ 
	
	}/* fin de classe ClockPanel */
	
	public void increment()
	{
		sec=sec+1;
		if(sec>60)
		{
			min=min+1;
			sec=1;
			if(min>60)
			{
				min=1;
				h=h+1;
				if(h>12)
				{
					h=1;
				}
			}
		}
		getClockPanel().repaint();
	}
		
	public ClockPanel getClockPanel() {
		//clockPanel.setBackground(Color.white);
		return clockPanel;
	}
	public void setClockPanel(ClockPanel clockPanel) {
		this.clockPanel = clockPanel;
	}

	private GregorianCalendar calend;
	private int h,min,sec,xc,yc;
	private ClockPanel clockPanel;
	private ClockThread clockThread;
	private Font font;
	
}/* fin de classe ClockApp*/

class ClockThread extends Thread{
	ClockThread(Horloge horloge)
	{
		this.horloge=horloge;
	}
	public void run()
	{
		while(true)
		{
			horloge.increment();
			try
			{
				sleep(1000);
			}
			catch(InterruptedException e)
			{
			}
				
		}
	}
	private Horloge horloge;
}
